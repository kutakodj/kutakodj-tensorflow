import datetime
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
import cv2 as cv
import keract
from tensorflow.keras.preprocessing import image

assert tf.__version__.startswith('2')


def visualize_channels(model, activations):
    layer_names = []
    for layer in model.layers[0:4]:
        layer_names.append(layer.name)  # Names of the layers, so you can have them as part of your plot

    images_per_row = 16

    for layer_name, layer_activation in zip(layer_names, activations):  # Displays the feature maps
        n_features = layer_activation.shape[-1]  # Number of features in the feature map
        size = layer_activation.shape[1]  # The feature map has shape (1, size, size, n_features).
        n_cols = n_features // images_per_row  # Tiles the activation channels in this matrix
        display_grid = np.zeros((size * n_cols, images_per_row * size))
        for col in range(n_cols):  # Tiles each filter into a big horizontal grid
            for row in range(images_per_row):
                channel_image = layer_activation[0,
                                :, :,
                                col * images_per_row + row]
                channel_image -= channel_image.mean()  # Post-processes the feature to make it visually palatable
                channel_image /= channel_image.std()
                channel_image *= 64
                channel_image += 128
                channel_image = np.clip(channel_image, 0, 255).astype('uint8')
                display_grid[col * size: (col + 1) * size,  # Displays the grid
                row * size: (row + 1) * size] = channel_image
        scale = 1. / size
        plt.figure(figsize=(scale * display_grid.shape[1],
                            scale * display_grid.shape[0]))
        plt.title(layer_name)
        plt.grid(False)
        plt.imshow(display_grid, aspect='auto', cmap='viridis')
        plt.show()


# https://github.com/philipperemy/keract
if __name__ == '__main__':
    _URL = "d:/BME/Felev_7/szakdolgozat/ermek.tgz"
    _file_name = "ermek.tgz"

    # get the file if it is from api
    # zip_file = tf.keras.utils.get_file(origin=_URL,
    #                                   fname=_file_name,
    #                                   extract=True)
    # base_dir = os.path.join(os.path.dirname(zip_file), 'ermek')

    # ermek has every coin with both sides in one folder, ermek2 has them in seperate ones
    base_dir = "d:/BME/Felev_7/szakdolgozat/ermek"

    IMAGE_SIZE = 224
    BATCH_SIZE = 64

    # Let's preprocess the images with Keras
    # We will also format the pics a little so the model doesn't see the same pic twice
    # (Good for preventing overfitting, and helping generalization)
    datagen = tf.keras.preprocessing.image.ImageDataGenerator(
        # Rotating a little
        rotation_range=40,
        # Translation of the given fraction of the total
        width_shift_range=0.2,
        height_shift_range=0.2,
        brightness_range=[0.2, 1.0],
        # Multiplication factor, Our original images consist in RGB coefficients in the 0-255,
        # but such values would be too high for our models to process (given a typical learning rate),
        # so we target values between 0 and 1 instead
        rescale=1. / 255,
        # Cutting
        shear_range=0.2,
        # Zooming in on pics
        zoom_range=0.2,
        # Flip pictures randomly horizontally
        horizontal_flip=False,
        vertical_flip=False,
        # Strategy used for filling in newly created pixels, which can appear after a rotation or a width/height shift
        fill_mode='nearest',
        # Fraction of data to reserve as validation, 20% is accepted generally
        validation_split=0.2
    )

    train_generator = datagen.flow_from_directory(
        base_dir,
        # All imgs are gonne be resized to this X * Y
        target_size=(IMAGE_SIZE, IMAGE_SIZE),
        #
        batch_size=BATCH_SIZE,
        # Which part of the split generated data is this training | validation
        subset='training'
    )

    val_generator = datagen.flow_from_directory(
        base_dir,
        target_size=(IMAGE_SIZE, IMAGE_SIZE),
        batch_size=BATCH_SIZE,
        subset='validation'
    )

    # We save the labels
    print(train_generator.class_indices)
    labels = '\n'.join(sorted(train_generator.class_indices.keys()))

    with open('labels.txt', 'w') as file:
        file.write(labels)

    IMG_SHAPE = (IMAGE_SIZE, IMAGE_SIZE, 3)

    base_model = tf.keras.applications.MobileNet(
        input_shape=IMG_SHAPE,
        # We do not want to include a fully connected layer at the top of the network
        # It would totally destroy the weight the model learned so far, so we would loose learned generalization
        include_top=False,
        # We want to use the pretrained weights from the imagenet model
        weights='imagenet'
    )

    # We freeze the convolutional base, we can use it then to extract the feature of the base model
    base_model.trainable = False

    # We create a classifier on top of the base conv. neural netw.
    model = tf.keras.models.Sequential([
        base_model,
        # We create a 2D conv layer, which is good for imgs
        # First param is filters, which is the num of output filters in the convolution
        # Second is kernel_size, which sets the 2D convolution window
        # We use rectified linear unit activation function, could try with other -> maybe Swish swish | relu
        tf.keras.layers.Conv2D(32, (3, 3), padding='same', activation='relu'),
        tf.keras.layers.MaxPooling2D((2, 2), strides=2),
        tf.keras.layers.Conv2D(32, (3, 3), padding='same', activation='relu'),
        # We add a dropout layer, which randomly sets input units to 0 with a frequency of the given rate param
        # This helps preventing overfitting
        # This layer is only applied when we are training the model
        tf.keras.layers.Dropout(0.2),
        # Global Average Pooling is an operation
        # that calculates the average output of each feature map in the previous layer.
        # Mostly used before final classification layers.
        tf.keras.layers.GlobalAveragePooling2D(),
        tf.keras.layers.Flatten(),
        # This is a regular dense connected layer
        # output = activation(dot(input, kernel) + bias)
        # softmax activation is preferred in multi-class classification problems
        # For erme2 -> 16, for erme -> 8
        tf.keras.layers.Dense(8, activation='softmax')
    ])

    # We need to compile the model before starting the training
    model.compile(
        # Every time a neural network finishes passing a batch through the network and generating prediction results,
        # it must decide how to use the difference between the results it got
        # and the values it knows to be true to adjust the weights on the nodes
        # so that the network steps towards a solution.
        # The algorithm that determines that step is known as the optimization algorithm.
        # Adam = Adaptive Moment Estimation
        # In addition to storing an exponentially decaying average of past squared gradients
        # it also keeps an exponentially decaying average of past gradients, similar to momentum.
        # Adam is basically RMSprop plus momentum.
        optimizer=tf.keras.optimizers.Adam(0.0001),
        # As part of the optimization algorithm,
        # the error for the current state of the model must be estimated repeatedly.
        # The choice of loss function is directly related to the activation function
        # used in the output layer of your neural network.
        loss='categorical_crossentropy',
        # Metric functions are similar to loss functions,
        # except that the results from evaluating a metric are not used when training the model.

        # The accuracy metric computes the accuracy rate across all predictions.
        # Categorical_accuracy metric computes the mean accuracy rate across all predictions.
        # Top_k_categorical_accuracy computes the top-k-categorical accuracy rate.
        # We take top k predicted classes from our model and see if the correct class was selected as top k.
        metrics=['accuracy']
    )

    # String summary of the network
    model.summary()
    print('Number of trainable variables = {}'.format(len(model.trainable_variables)))

    # Visualize one output of one layer

    # layer_outputs = [layer.output for layer in model.layers[1:8]]
    # activation_model = tf.keras.models.Model(inputs=model.input, outputs=layer_outputs)
    # x_batch, y_batch = next(train_generator)
    # image_tensor = image.img_to_array(x_batch[0])
    # image_tensor = np.expand_dims(image_tensor, axis=0)
    # activations = activation_model.predict(image_tensor)
    #
    # first_layer_activation = activations[0]
    # print(first_layer_activation.shape)
    # plt.matshow(first_layer_activation[0, :, :, 4], cmap='viridis')
    # plt.show()

    # Visualize outputs for the image processing layers

    # visualize_channels(model, activations)

    epochs = 30

    # We get a history object that holds all information of the training process
    #
    history = model.fit(
        train_generator,
        # Total number of steps (batches of samples) before declaring one epoch finished and starting the next epoch.
        steps_per_epoch=len(train_generator),
        # Number of epochs to train the model.
        epochs=epochs,
        # Data on which to evaluate the loss and any model metrics at the end of each epoch.
        # The model will not be trained on this data.
        validation_data=val_generator,
        # Only relevant if steps_per_epoch is specified.
        # Total number of steps (batches of samples) to validate before stopping.
        validation_steps=len(val_generator)
    )

    # Plotting learning curves
    acc = history.history['accuracy']
    val_acc = history.history['val_accuracy']

    loss = history.history['loss']
    val_loss = history.history['val_loss']

    plt.figure(figsize=(8, 8))
    plt.subplot(2, 1, 1)
    plt.plot(acc, label='Training Accuracy')
    plt.plot(val_acc, label='Validation Accuracy')
    plt.legend(loc='lower right')
    plt.ylabel('Accuracy')
    plt.ylim([min(plt.ylim()), 1])
    plt.title('Training and Validation Accuracy')

    plt.subplot(2, 1, 2)
    plt.plot(loss, label='Training Loss')
    plt.plot(val_loss, label='Validation Loss')
    plt.legend(loc='upper right')
    plt.ylabel('Cross Entropy')
    plt.ylim([0, 1.0])
    plt.title('Training and Validation Loss')
    plt.xlabel('epoch')

    filename = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")

    plt.savefig(f'd:/BME/Felev_7/szakdolgozat/diagramok/{filename}.png')
    plt.show()

    saved_model_dir = f'd:/BME/Felev_7/szakdolgozat/models/{filename}'
    tf.saved_model.save(model, saved_model_dir)

    converter = tf.lite.TFLiteConverter.from_saved_model(saved_model_dir)
    tflite_model = converter.convert()

    with open(f'd:/BME/Felev_7/szakdolgozat/tflitemodels/{filename}.tflite', 'wb') as f:
        f.write(tflite_model)
