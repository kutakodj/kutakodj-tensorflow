import datetime
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf

assert tf.__version__.startswith('2')

if __name__ == '__main__':
    _URL = "d:/BME/Felev_7/szakdolgozat/ermek.tgz"
    _file_name = "ermek.tgz"

    # get the file if it is from api
    # zip_file = tf.keras.utils.get_file(origin=_URL,
    #                                   fname=_file_name,
    #                                   extract=True)
    # base_dir = os.path.join(os.path.dirname(zip_file), 'ermek')

    base_dir = "d:/BME/Felev_7/szakdolgozat/ermek_edge"

    IMAGE_SIZE = 224
    BATCH_SIZE = 64

    # Let's preprocess the images with Keras
    # We will also format the pics a little so the model doesn't see the same pic twice
    # (Good for preventing overfitting, and helping generalization)
    datagen = tf.keras.preprocessing.image.ImageDataGenerator(
        # Rotating a little
        rotation_range=40,
        # Translation of the given fraction of the total
        width_shift_range=0.2,
        height_shift_range=0.2,
        # Multiplication factor, Our original images consist in RGB coefficients in the 0-255,
        # but such values would be too high for our models to process (given a typical learning rate),
        # so we target values between 0 and 1 instead
        rescale=1. / 255,
        # Cutting
        shear_range=0.2,
        # Zooming in on pics
        zoom_range=0.2,
        # Flip pictures randomly horizontally
        horizontal_flip=False,
        vertical_flip=False,
        # Strategy used for filling in newly created pixels, which can appear after a rotation or a width/height shift
        fill_mode='nearest',
        # Fraction of data to reserve as validation, 20% is accepted generally
        validation_split=0.2
    )

    train_generator = datagen.flow_from_directory(
        base_dir,
        # All imgs are gonne be resized to this X * Y
        target_size=(IMAGE_SIZE, IMAGE_SIZE),
        #
        batch_size=BATCH_SIZE,
        # Which part of the split generated data is this training | validation
        subset='training'
    )

    val_generator = datagen.flow_from_directory(
        base_dir,
        target_size=(IMAGE_SIZE, IMAGE_SIZE),
        batch_size=BATCH_SIZE,
        subset='validation'
    )

    # We save the labels
    print(train_generator.class_indices)
    labels = '\n'.join(sorted(train_generator.class_indices.keys()))

    with open('labels.txt', 'w') as file:
        file.write(labels)

    IMG_SHAPE = (IMAGE_SIZE, IMAGE_SIZE, 3)

    base_model = tf.keras.applications.MobileNetV2(
        input_shape=IMG_SHAPE,
        # We do not want to include a fully connected layer at the top of the network
        # It would totally destroy the weight the model learned so far, so we would loose learned generalization
        include_top=False,
        # We want to use the pretrained weights from the imagenet model
        weights='imagenet'
    )

    # We freeze the convolutional base, we can use it then to extract the feature of the base model
    base_model.trainable = False

    # We create a classifier on top of the base conv. neural netw.
    model = tf.keras.Sequential([
        base_model,
        # We create a 2D conv layer, which is good for imgs
        # First param is filters, which is the num of output filters in the convolution
        # Second is kernel_size, which sets the 2D convolution window
        # We use rectified linear unit activation function, could try with other -> maybe Swish swish | relu
        tf.keras.layers.Conv2D(32, 3, activation='swish'),
        # We add a dropout layer, which randomly sets input units to 0 with a frequency of the given rate param
        # This helps preventing overfitting
        # This layer is only applied when we are training the model
        tf.keras.layers.Dropout(0.2),
        # Global Average Pooling is an operation
        # that calculates the average output of each feature map in the previous layer.
        # Mostly used before final classification layers.
        tf.keras.layers.GlobalAveragePooling2D(),
        # This is a regular dense connected layer
        # output = activation(dot(input, kernel) + bias)
        # softmax activation is preferred in multi-class classification problems
        tf.keras.layers.Dense(8, activation='softmax')
    ])

    # We need to compile the model before starting the training
    model.compile(
        # Every time a neural network finishes passing a batch through the network and generating prediction results,
        # it must decide how to use the difference between the results it got
        # and the values it knows to be true to adjust the weights on the nodes
        # so that the network steps towards a solution.
        # The algorithm that determines that step is known as the optimization algorithm.
        # Adam = Adaptive Moment Estimation
        # In addition to storing an exponentially decaying average of past squared gradients
        # it also keeps an exponentially decaying average of past gradients, similar to momentum.
        # Adam is basically RMSprop plus momentum.
        optimizer='adam',
        # As part of the optimization algorithm,
        # the error for the current state of the model must be estimated repeatedly.
        # The choice of loss function is directly related to the activation function
        # used in the output layer of your neural network.
        loss='categorical_crossentropy',
        # Metric functions are similar to loss functions,
        # except that the results from evaluating a metric are not used when training the model.

        # The accuracy metric computes the accuracy rate across all predictions.
        # Categorical_accuracy metric computes the mean accuracy rate across all predictions.
        # Top_k_categorical_accuracy computes the top-k-categorical accuracy rate.
        # We take top k predicted classes from our model and see if the correct class was selected as top k.
        metrics=['accuracy']
    )

    # String summary of the network
    model.summary()
    print('Number of trainable variables = {}'.format(len(model.trainable_variables)))

    epochs = 30

    # We get a history object that holds all information of the training process
    #
    history = model.fit(
        train_generator,
        # Total number of steps (batches of samples) before declaring one epoch finished and starting the next epoch.
        steps_per_epoch=len(train_generator),
        # Number of epochs to train the model.
        epochs=epochs,
        # Data on which to evaluate the loss and any model metrics at the end of each epoch.
        # The model will not be trained on this data.
        validation_data=val_generator,
        # Only relevant if steps_per_epoch is specified.
        # Total number of steps (batches of samples) to validate before stopping.
        validation_steps=len(val_generator)
    )

    # Plotting learning curves
    acc = history.history['accuracy']
    val_acc = history.history['val_accuracy']

    loss = history.history['loss']
    val_loss = history.history['val_loss']

    plt.figure(figsize=(8, 8))
    plt.subplot(2, 1, 1)
    plt.plot(acc, label='Training Accuracy')
    plt.plot(val_acc, label='Validation Accuracy')
    plt.legend(loc='lower right')
    plt.ylabel('Accuracy')
    plt.ylim([min(plt.ylim()), 1])
    plt.title('Training and Validation Accuracy')

    plt.subplot(2, 1, 2)
    plt.plot(loss, label='Training Loss')
    plt.plot(val_loss, label='Validation Loss')
    plt.legend(loc='upper right')
    plt.ylabel('Cross Entropy')
    plt.ylim([0, 1.0])
    plt.title('Training and Validation Loss')
    plt.xlabel('epoch')

    filename = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")

    plt.savefig(f'd:/BME/Felev_7/szakdolgozat/diagramok/{filename}.png')
    plt.show()

    saved_model_dir = f'd:/BME/Felev_7/szakdolgozat/models/{filename}'
    tf.saved_model.save(model, saved_model_dir)

    converter = tf.lite.TFLiteConverter.from_saved_model(saved_model_dir)
    tflite_model = converter.convert()

    with open(f'd:/BME/Felev_7/szakdolgozat/tflitemodels/{filename}.tflite', 'wb') as f:
        f.write(tflite_model)
