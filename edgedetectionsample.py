import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
    # Load the image
    image_original = cv.imread('d:/BME/Felev_7/szakdolgozat/ermek/50_forintos/hungary-50-forint-1993.jpg', cv.IMREAD_COLOR)
    # Convert image to gray scale
    image_gray = cv.cvtColor(image_original, cv.COLOR_BGR2GRAY)

    # Reduce noise in image
    img = cv.GaussianBlur(image_gray, (3, 3), 0)

    # 3x3 Y-direction  kernel
    sobel_y = np.array([[-1, -2, -1], [0, 0, 0], [1, 2, 1]])
    # 3 X 3 X-direction kernel
    sobel_x = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]])

    # Filter the image using filter2D, which has inputs: (grayscale image, bit-depth, kernel)
    filtered_image = cv.Laplacian(img, ksize=3, ddepth=cv.CV_16S)
    # filtered_image = cv.Canny(img, threshold1=20, threshold2=150)
    # converting back to uint8
    filtered_image = cv.convertScaleAbs(filtered_image)

    # Plot outputs
    (fig, (ax1, ax2)) = plt.subplots(1, 2, figsize=(15, 15))
    ax1.title.set_text('Original Image')
    ax1.imshow(image_original)

    ax2.title.set_text('Laplacian Filtered Image')
    ax2.imshow(filtered_image, cmap='gray')
    plt.show()