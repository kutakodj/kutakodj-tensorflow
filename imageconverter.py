import cv2
import numpy as np
import glob
import os
import matplotlib.pyplot as plt


def auto_canny(image, sigma=0.33):
    image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    img = cv2.GaussianBlur(image_gray, (3, 3), 0)
    pixel_intensities_median = np.median(img)
    lower = int(max(0, (1.0 - sigma) * pixel_intensities_median))
    upper = int(min(255, (1.0 + sigma) * pixel_intensities_median))
    return cv2.Canny(image, lower, upper)


def laplacian(image):
    image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    img = cv2.GaussianBlur(image_gray, (3, 3), 0)
    filtered_image = cv2.Laplacian(img, ksize=3, ddepth=cv2.CV_16S)
    filtered_image = cv2.convertScaleAbs(filtered_image)
    return filtered_image


def show_image(image, detector):
    if detector == 'laplacian':
        plt.imshow(laplacian(image))
    elif detector == 'canny':
        plt.imshow(auto_canny(image))
    plt.show()


if __name__ == '__main__':
    base_dir = "d:/BME/Felev_7/szakdolgozat/ermek"
    save_dir = "d:/BME/Felev_7/szakdolgozat/ermek_edge"
    for root, dirs, files in os.walk(base_dir):
        path = root.split(os.sep)
        print((len(path) - 1) * '----', os.path.basename(root))
        print(f'{os.path.basename(root)}/')
        images = []
        for file in glob.glob(f'{path[0]}/{os.path.basename(root)}/*.*'):
            images.append(cv2.imread(file, cv2.IMREAD_COLOR))
        number = 0
        for image in images:
            # show_image(image, 'laplacian')
            print(f'{save_dir}/{os.path.basename(root)}/canny_{number}.png')
            filtered = laplacian(image)
            suc = cv2.imwrite(os.path.join(f'{save_dir}/{os.path.basename(root)}/', f'canny_{number}.png'), filtered)
            print(suc)
            number += 1
